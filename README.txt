Code accompanying the paper 
"Superelliptical laws for complex networks"
by Stefano Allesina, Elizabeth Sander, Matthew J. Smith & Si Tang

License:
--------
The code is made available under the GNU GPL license:
http://www.gnu.org/licenses/gpl.html

Requests:
---------
Please send any request/bug/issue/question to
Stefano Allesina sallesina@uchicago.edu

Citation:
---------
If you use the code for your work, please reference the paper:
Allesina, S., Sander, E., Smith M.J. & Tang, S., 2013.
"Superelliptical laws for complex networks"


Structure of the code:
----------------------
Two files are provided. Both are for the statistical software R (http://cran.us.r-project.org/).
They require the following packages:
- igraph (http://igraph.sourceforge.net/, http://cran.us.r-project.org/web/packages/igraph/)
- MASS (http://cran.us.r-project.org/web/packages/MASS/)
- ggplot2 (http://had.co.nz/ggplot2/, http://cran.us.r-project.org/web/packages/ggplot2/)

------------------
Superelliptical.R
------------------
In the file Superelliptical.R, you find the routines needed to build, fit and plot the eigenvalue distribution of matrices with complex network structure and nonzero entries sampled from a bivariate normal distribution with correlation rho. 

Usage (in R):
> source("Superelliptical.R")
> Superelliptical(1000, 10, 1, 'power-law')
> Superelliptical(1250, 25, 0, 'k-regular')

For details on the use, load the file in R, or browse the code.

------------------
PredictLambda1.R
------------------
Routines needed to build, fit and plot the eigenvalue distribution of adjacency matrices with complex network structure.

Usage (in R):
> source("PredictLambda1.R")
> PredictLambda1(1500, 50, type = 'power-law')
> PredictLambda1(1000, 10, type = 'k-regular')

For details on the use, load the file in R, or browse the code.

